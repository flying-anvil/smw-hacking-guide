# Summary

# Chapters I guess

- [Introduction](./introduction.md)

- [Custom Graphics](./custom-graphics/index.md)
  - [Blocks](./custom-graphics/blocks.md)

# Technical Documentation

- [Files](./documentation/files/index.md)
  - [Palette (.pal)](./documentation/files/palette.md)
    - [Palette Mask (.palmask)](./documentation/files/palette_mask.md)
  - [Tile Map 8x8 (.bin / .gfx)](./documentation/files/tile_map_8x8.md)
- [Graphics](./documentation/graphics/index.md)

[]()

---

[Thing on bottom]()
