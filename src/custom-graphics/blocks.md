# Creating Custom Block Graphics

<!-- Maybe move most of this also into "Technical Documentation"/Blocks -->

Blocks are 16x16 tiles that form what we might call a "well designed level".
However, the graphics are not stored in a 16x16 format.
Instead, a block is made up of four 8x8 graphics.  
I refer to 8x8 tiles simply as "tiles" and to whole 16x16 blocks as "meta-tiles" or just "blocks".

Let's take a look at a simple example block (![Example Block](../common/blocks/golden-cheese-block.png)).  
It consists of four 8x8 graphics, one for each corner:  

![Example block split up into individual tiles](./img/golden-cheese-block_8x8_split.png)

This is common for pretty much all blocks in SMW.

However, the way graphics are displayed and how they are stored are decoupled.
Often you can find each tile stored in nor particular order or next to each other like this:

![Example block split graphics](./img/golden-cheese-block_8x8_layout.png)

SMW then contains a mapping that tells each block which tiles it should use for each corner,
allowing separated graphics to be stitched together.

This mapping is known as Map16 and has another trick up its sleeve:
It can tell each tile to be individually flipped, either vertically, horizontally or both.
By utilizing that we can potentially save some precious graphics space.

Take this block as an example ![Lantern](../common//blocks/lantern.png).
Only one quarter of its graphics needs to be stored:

![Lantern top-left](./img/lantern-top-left.png)

We can then tell Map16 to use the same tile for the other corners, but flipped.

![Lantern whole](./img/lantern-whole.png)

This is not restricted to all tiles being re-used though.
This orb (![Orb](../common/blocks/orb.png)) can be stored with 3 unique graphics
with the missing 4th tile being a flipped graphic of the top-right tile:  
<sup>(_the missing tile has been made faint for clarity_)</sup>

![Orb unique graphics](./img/orb-uniques.png)

## Colors

SMW uses palettes for colors. This is the palette used in level 105:

![Palette level 105](../common/palette_105.png)

---

![Template](./img/Structured_Template_Indexed.png)
