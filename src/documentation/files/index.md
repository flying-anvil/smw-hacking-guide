# Documentation of files related to SMW hacking

Each of the following chapters contain some information about common files.
They contain a short, general description of the file type, a short summary of technical characteristics
and a detailed description about the format.

They also might include some caveats and a short example/pseudo-code on how to parse the file.
**Note that examples might not include neither optimizations, special language features
nor cover error-handling or caveats for the sake of clarity!**
