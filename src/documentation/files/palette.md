# Palette File (.pal)

Lunar Magic has the ability to export and import color palettes to/from files.
Such files typically use the `.pal` file extension.
Many custom graphics you can find on SMWCentral come with palettes included
(they might also come with a palette mask).

The SNES color palette has space for 256 colors.
SMW specifically splits these apart into 16 smaller palettes á 16 colors each.
A palette file that contains exactly 256 colors is 768 bytes big (256 * 3).

## Summary

- File Extension: `.pal`
- File Size: 768 bytes*
- Contains: Color information
- Parsing difficulty: Easy

## File Structure

Such exported palettes are very simple and simply contain 8-bit RGB values.
Each byte represents a single color channel.
To read a single color, read 3 consecutive bytes and treat each byte as a single color channel.

Example:

```hex
88 58 18 D8 A0 38 F8 D8 70
```

This example contains 9 bytes, meaning 3 colors.

The first color has a red value of 0x88, a green value of 0x58 and a blue value of 0x18.
The whole color could be written as `#885818`. The next 3 bytes are the 2nd color,
the next 3 bytes after that the 3rd color and so on.

## Caveats

When the file contains less than 256 colors, Lunar Magic imports the colors that are present.
If a color is incomplete (like when the file is 10 bytes long), Lunar Magic only imports colors that are complete.

Files containing more than 256 colors are also fine, but Luna Magic only imports the first 256 colors.

Palettes do not include information about transparency. The first color of each sub-palette is treaded as fully transparent in SMW.
Thus they are typically full-black in palette files. When exporting from Lunar Magic, they are set to the color of the "Back Area Color".

## Parsing

```rust
/// Each color channel is 8-bit
struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

/// Ignores colors that are cut-off
fn read_pal_bytes(file_bytes: Vec<u8>) -> Vec<Color> {
    let mut colors = Vec::new();

    for i in 0..file_bytes.len() / 3 {
        let red   = file_bytes[i * 3 + 0];
        let green = file_bytes[i * 3 + 1];
        let blue  = file_bytes[i * 3 + 2];

        colors.push(Color{red, green, blue});
    }

    return colors;
}

fn write_pal_bytes(colors: Vec<Color>) -> Vec<u8> {
    let mut bytes = Vec::new();

    for color in colors {
        bytes.push(color.red);
        bytes.push(color.green);
        bytes.push(color.blue);
    }

    return bytes;
}
```

### Implementations

- [GFX-DeDuplicator (Read)](https://gitlab.com/flying-anvil/rust/gfx-deduplicator/-/blob/2ba897e5a7f7fd02e683a070c956859d8ff8a982/src/value/palette.rs#L147)
  - This implementation strictly separates a palette file into 16 sub-palettes, creating a collection of palettes.
