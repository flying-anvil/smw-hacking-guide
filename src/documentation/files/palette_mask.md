# Palette File (.pal)

To be able to export only certain colors from a palette, a palette mask can be used.

Graphics from SMWCentral that include a custom palette are likely to also include
a palette mask to only overwrite relevant colors.

## Summary

- File Extension: `.palmask`
- File Size: 256 bytes*
- Contains: Mask for which colors to include when importing
- Parsing difficulty: Easy

## File Structure

Each bytes in a palette mask is either `0x00` or `0x01`, meaning "Do not overwrite" and "Do overwrite".
As each sub-palette contains 16 colors, every 16 bytes in a mask covers one sub-palette, one byte per color.

Example:

```hex
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01
01 01 01 01 01 01 01 01 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

The above example means, that every color in the first sub-palette should be ignored when importing,
include all colors from the second sub-palette,
and include only the first 8 colors from the third sub-palette.

## Caveats

Various tools might interpret values other than 0 and 1 differently.

## Parsing

```rust
struct PaletteMask(Vec<bool>);

fn read_palette_mask_bytes(bytes: Vec<u8>) -> PaletteMask {
    let mut mask = Vec::new();

    for byte in bytes {
        // Treat every value other than 0 as "include"
        mask.push(byte > 0x00);
    }

    return PaletteMask(mask);
}

fn write_palette_mask_bytes(mask: PaletteMask) -> Vec<u8> {
    let mut bytes = Vec::new();

    for include in mask.0 {
        bytes.push(if include { 0x01 } else { 0x00 });
    }

    return bytes;
}
```

### Implementations

- _none yet_
