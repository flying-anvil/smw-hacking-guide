# Template _TODO_ (._TODO_)

_TODO_

## Summary

- File Extension: `.TODO`
- File Size: _TODO_
- Contains: _TODO_
- Parsing difficulty: _TODO_

## File Structure

_TODO_

Example:

```hex
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

## Caveats

## Parsing

```rust
fn read_bytes(bytes: Vec<u8>) -> _TODO_ {}
fn write_bytes(_todo_: _TODO_) -> Vec<u8> {}
```

### Implementations

- _none yet_

## References

- 
