# Tile Map 8x8 (.bin / .gfx)

These files contain graphics (gfx) information.

For an explanation on how graphics work, see [Graphics](../graphics/index.md)

## Summary

- File Extension: `.bin` or `.gfx`
- File Size: _varies_
  - Technically, each multiple of 32 bytes is valid
  - The most common size in SMW is 4 KiB (4096 bytes)
- Contains: Graphics (with color indices)
- Parsing difficulty: Medium

## File Structure

I'm only covering the 4BPP (4 Bits Per Pixel) format (for now).

As the name suggests, each pixel has 4 bits, thus having 16 (2⁴) colors indices to choose from.
One might assume that every 4 consecutive bits is one pixel (2 pixels per byte). However, that's not the case.
For the hardware architecture of the SNES it's better to store them as 4 separate, bi‑intertwined bitplanes.

_TODO_: Add an image that shows each bitplane of the mini-orb.

Each bitplane is 1 byte / 8 bits long, thus fitting perfectly in a row.
The bitplanes with the lowest bits are stored first.

Example:

For simplicity, let's take a look at only a single row. These are the 4 bitplanes:
<sup>(_with their respective magnitude on the right_)</sup>

```
1 0 1 0 0 0 0 1 │ 1
0 1 1 1 1 0 1 0 │ 2
0 0 0 0 0 1 0 0 │ 4
0 0 0 0 0 0 0 0 │ 8
```

When combining those 4 bitplanes, it becomes `1 2 3 2 2 4 2 1`, each number representing a pixel/index of a color palette.

_TODO_: Add graphics to these bitplanes. This example is from the mini orb.

---

8x8 tiles are stored consecutively and each tile is 32 bytes long. So far so simple.

Byte 1 of each tile contains the first/lowest bitplane of the first row, byte 2 contains the second bitplane of the first row.
The 3rd and 4th bitplanes are at bytes 17 and 18 respectively.
The second row is then offset by 2 bytes (bytes 3, 4, 19, 20) and so on.

In other words, all bitplanes 1 and 2 of all rows are stored in the first half
and all bitplanes 3 and 4 are stored in the second half.
First come all the (1st and 2nd) bitplanes in bytes 1 to 16, then come all the (3rd and 4th) bitplanes in bytes 17 to 32.

Here's an example of a tile. The letter represents the row and the number the bitplane.

```
A1 A2 B1 B2 C1 C2 D1 D2
E1 E2 F1 F2 G1 G2 H1 H2
A3 A4 B3 B4 C3 C4 D3 D4
E3 E4 F3 F4 G3 G4 H3 H4
```

## Caveats

## Parsing

```rust
struct Tile {
  /// An array with 64 bytes (8x8 = 64)
  color_indices: [u8; 64],
}

fn read_bytes(bytes: Vec<u8>) -> Vec<Tile> {
  // This vector will have (bytes.len() / 32) elements
  let mut tiles = Vec::new();

  // Each tile is 32 bytes in size
  bytes.chunks(32).for_each(|chunk| {
    // A tile contains 8x8 = 64 values (color indices)
    let mut color_indices = [0u8; 64];

    for y in 0..8 {
      let row_offset = y * 2;

      let bitplane_1 = chunk[row_offset + 0];
      let bitplane_2 = chunk[row_offset + 1];
      let bitplane_3 = chunk[row_offset + 16];
      let bitplane_4 = chunk[row_offset + 17];

      // Iterate each pixel in a row and read the corresponding bit from the bitplane
      for x in 0..8 {
        let mask = 1 << x;
        let bit_1 = (bitplane_1 & mask) >> x;
        let bit_2 = (bitplane_2 & mask) >> x;
        let bit_3 = (bitplane_3 & mask) >> x;
        let bit_4 = (bitplane_4 & mask) >> x;

        // Combine the bitplane-bits by OR-ing them together
        //   1 0 0 0
        // | 0 1 0 0
        // | 0 0 1 0
        // | 0 0 0 1
        // = 1 1 1 1
        let combined = bit_4 << 3 | bit_3 << 2 | bit_2 << 1 | bit_1 << 0;
        color_indices[y * 8 + (7 - x)] = combined;
      }
    }

    tiles.push(Tile{color_indices});
  });

  return tiles;
}

/// It's a bit messy and can probably be made more clear
fn write_bytes(tiles: Vec<Tile>) -> Vec<u8> {
  // This vector will have (tiles.len() * 32) elements
  let mut bytes = Vec::new();

  for tile in tiles {
    // A small intermediate step for clarity; will contain the 32 bytes of the current tile
    let mut tile_bytes = Vec::new();

    // tile.lines() are 8 chunks of 8 bytes each.
    for (y, line) in tile.lines().iter().enumerate() {
      let mut bitplanes = Vec::new();
      for i in 0..4 {
        bitplanes.push(0);

        for (x, color_index) in line.iter().enumerate() {
          let read_bit = color_index & (1 << i);
          // The read bit might be this for the second bitplane: 0100. Now we need to move it to the x position.
          // First, move that bit the the far-right, then left to the corresponding x-index
          bitplanes[i] |= (read_bit >> i) << (7 - x);
        }
      }

      let offset = y * 2;
      tile_bytes[offset + 0]  = bitplanes[0];
      tile_bytes[offset + 1]  = bitplanes[1];
      tile_bytes[offset + 16] = bitplanes[2];
      tile_bytes[offset + 17] = bitplanes[3];
    }

    bytes.push_vec(tile_bytes);
  }
}
```

### Implementations

- [GFX-DeDuplicator (Read)](https://gitlab.com/flying-anvil/rust/gfx-deduplicator/-/blob/2ba897e5a7f7fd02e683a070c956859d8ff8a982/src/parser.rs#L4)
  - Should be easy to understand. Used as a base for the above parsing example.
- [GFX-DeDuplicator (Write)](https://gitlab.com/flying-anvil/rust/gfx-deduplicator/-/blob/b2e99a5e59ce6cb06da88e7187237c9ccbb6ef83/src/parser.rs#L47)
  - Is a bit hacky and not very clear <small><small><small>(sorry)</small></small></small>.

## References

- <https://georgjz.github.io/snesaa03>
- <https://sneslab.net/wiki/Graphics_Format>
