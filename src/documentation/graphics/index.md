# Graphics

Unlike common modern graphics formats (like png or bmp), SNES graphics do not contain color information.
Instead, they only contain references to colors in a color palette, which is called color indexing.
(_Technically, png files also support a similar feature, although that's not widely used._)

To display the contents of such files, we need to also provide a color palette to lookup colors from.

Graphics are stored in tiles with 8x8 pixels/colors/indices. To make larger graphics, multiple tiles are used.
For example a block in SMW is made up of 4 8x8 tiles.

<details>
  <summary>Show some examples</summary>

  Let's say we want to have an image of a simple heart.
  To keep this example simple, let's keep the colors to a minimum of 2, being transparent/background and black.
  This graphic only contains 0 or 1 for each pixel. The attached palette defines 0 as grey and 1 as black.

  ![Example graphic of a heart](./img/indexed-graphics.png)
</details>
